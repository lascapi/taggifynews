# taggifynews

Get the news feed and show tags from that

## Needed
- NodeJS
- Express : https://expressjs.com/ -> `npm install express --save`
- XMLHttpRequest : `npm i xmlhttprequest`
- body-parser : `npm install body-parser`
## Installation
1. Clone the project where you want. 
1. `npm init -y`
1. `node taggifynews.js`
