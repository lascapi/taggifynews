//requires
const express = require('express')
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest
//const bodyParser = require('body-parser')


//configuration
const app = express()
const port = 3000
let articles = 'Hello World! :)'

//routes
//home
app.get('/', (req, res) => res.send(articles))
//secrets
app.all('/secret', function (req, res, next) {
    console.log('Accessing the secret section ...')
    next() // pass control to the next handler
})
//todos
app.get('/todos', (req, res) => res.send('list de todos'))

//Start the server
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))

// AJAX Request
// use XMLHttpRequest Object from JS API
let xhr = new XMLHttpRequest()
// for now nothing happened
// request configuration
xhr.open(
    "get",
    "https://cloud.isman.fr/index.php/apps/news/export/articles",
    true,
    'pascal',
    'cuireconcentrer'
)
// configure listeners
xhr.onreadystatechange = function() {
    // if done and if status ok
    if (xhr.readyState === 4 && xhr.status === 200) {
        console.log("articles are downloaded")
        articles = JSON.parse(xhr.responseText)
    } else if (xhr.readyState === 4) {
        console.log("articles : ERROR")
        articles = xhr.getAllResponseHeaders()
    }
}

// execute request
xhr.send(); // async treatment
console.log("request is sent")